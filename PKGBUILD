# Maintainer: Guillaume ALAUX <guillaume@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Leif Warner <abimelech@gmail.com>

pkgname=sbt
epoch=1
pkgver=1.10.10
pkgrel=1
pkgdesc='The interactive build tool'
arch=('any')
url='https://www.scala-sbt.org/'
license=('Apache-2.0')
_jdkver=8
depends=('java-runtime-headless' 'sh')
makedepends=('sbt' 'git' "jdk${_jdkver}-openjdk")
optdepends=('rsync: offline installation support')
install=sbt.install
options=(!debug)
source=(git+https://github.com/sbt/sbt.git#tag=v${pkgver})
validpgpkeys=('2EE0EA64E40A89B84B2DF73499E82A75642AC823') # sbt build tool
sha256sums=('da7f0d0f147bc0eb9256c5be80f503ce09e443e21c6fbe6f2b999f0702f809dc')
b2sums=('78a5ac976342de78e21c9730b63f87cc9bafe838c366b1e28a93ac28d9f069488a9cd3ff41e0ed84471d081c1307e1aa72eeb7dd6855df362a13f894bead1355')
backup=(etc/${pkgname}/sbtopts)

build() {
  cd "${pkgname}/launcher-package"
  export JAVA_HOME="/usr/lib/jvm/java-$_jdkver-openjdk"
  export PATH="/usr/lib/jvm/java-$_jdkver-openjdk/bin:$PATH"
  sbt \
    -Dsbt.build.version=${pkgver} \
    -Dsbt.build.offline=false \
    clean \
    universal:packageBin \
    universal:packageZipTarball
  tar -xf target/universal/sbt.tgz -C "${srcdir}"
}

check() {
  cd "${pkgname}"
  export JAVA_HOME="/usr/lib/jvm/java-$_jdkver-openjdk"
  export PATH="/usr/lib/jvm/java-$_jdkver-openjdk/bin:$PATH"
  sbt test
}

package() {
  cd "${pkgname}"

  mkdir -p "${pkgdir}"/usr/share/${pkgname}
  cp -r bin "${pkgdir}"/usr/share/${pkgname}
  rm "${pkgdir}"/usr/share/${pkgname}/bin/*{.bat,.exe,-darwin}
  chmod -x "${pkgdir}"/usr/share/${pkgname}/bin/*
  chmod +x "${pkgdir}"/usr/share/${pkgname}/bin/{sbt,sbtn-${CARCH}-pc-linux}
  mkdir -p "${pkgdir}"/usr/bin
  ln -s /usr/share/${pkgname}/bin/sbt "${pkgdir}"/usr/bin/sbt

  mkdir -p "${pkgdir}/etc"
  cp -r conf "${pkgdir}"/etc/${pkgname}
  rm "${pkgdir}"/etc/${pkgname}/sbtconfig.txt
  ln -s /etc/${pkgname} "${pkgdir}"/usr/share/${pkgname}/conf

  install -D LICENSE -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
